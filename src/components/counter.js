import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { increment,decrement } from "../stateManagments/Slices/counter/counterSlice";

function Counter(){
    const count = useSelector((state)=>state.counter.value)
    const dispatch = useDispatch()
    return(
        <div>
            value:{count}
            <br/>
            <button onClick={()=>dispatch(increment())}>increment</button>
            <button onClick={()=>dispatch(decrement())}>decrement</button>
        </div>
    );
}
export default Counter;