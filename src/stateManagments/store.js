import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "./Slices/counter/counterSlice";
export const store = configureStore({
    reducer:{
        counter:counterSlice,
    },
})