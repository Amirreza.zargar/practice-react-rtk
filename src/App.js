import { Provider } from 'react-redux';
import './App.css';
import { store } from './stateManagments/store';
import Counter from './components/counter';

function App() {
  return (
      <Provider store={store}>
          <div className='App'>
              <h1>Redux Toolkit</h1>
              <Counter/>
          </div>
      </Provider>
  );
}

export default App;
